#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <epicsStdio.h>
#include <stdlib.h>

#include "http.h"

#define BUFFER_SIZE 5121

const char *post_url;
const char *post_var;
const char *post_filename;

static const char *getCheckEnv(const char *varname, const char *caller, const char *default_value)
{
    const char *var = getenv(varname);
    if (var == NULL || var[0] == '\0')
    {
        printf("%s: %s not set. Defaults to \"%s\"\n", caller, varname, default_value);
        var = default_value;
    }
    return var;
}

static long subSendFileInit(aSubRecord *prec)
{
    post_url = getCheckEnv("POST_URL", "subSendFileInit", "index.html");
    post_var = getCheckEnv("POST_VAR", "subSendFileInit", "filename");
    post_filename = getCheckEnv("POST_FILENAME", "subSendFileInit", "file.txt");
    return 0;
}

static long subSendFileProc(aSubRecord *prec)
{
    int status;

    post_url = getCheckEnv("POST_URL", "subSendFileInit", "index.html");
    post_var = getCheckEnv("POST_VAR", "subSendFileInit", "filename");
    post_filename = getCheckEnv("POST_FILENAME", "subSendFileInit", "file.txt");

    httpVar_t filename = {post_var, post_filename};
    status = httpSendFile(prec->a, *(epicsInt32 *)prec->b, post_url, filename, prec->c, prec->vala, BUFFER_SIZE - 1);

    *(int *)prec->valb = httpGetResponseCode(prec->vala);

    return status;
}

epicsRegisterFunction(subSendFileInit);
epicsRegisterFunction(subSendFileProc);