* When we call drvAsynIPPortConfigure(PORT, HOST, ...), this registers the port by calling registerPort() from asynManager.c
* When we next call pasynOctetSyncIO->connect(port, addr, &pasynUser, NULL), this looks for the existing port, and then tries
  to add a new device at the address addr. The resulting information is placed in pasynUser. This is done by calling
  connectDevice() from asynManager.c
* It appears that to single port we can have multiple devices, specified by integer addresses. i.e. the 24 in the example is
  entirely arbitrary.