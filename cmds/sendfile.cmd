# This should be a test startup script
require http

epicsEnvSet("IOCNAME", "TEST")
epicsEnvSet("PORT", "L0")
epicsEnvSet("ADDRESS", "24")

iocshLoad("$(http_DIR)/sendfile.iocsh")

#asynSetTraceIOMask("$(PORT)",$(ADDRESS),7)
asynSetTraceMask("$(PORT)",$(ADDRESS),63)

iocInit()
